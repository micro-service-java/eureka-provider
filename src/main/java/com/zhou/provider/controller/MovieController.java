package com.zhou.provider.controller;

import com.zhou.provider.model.Movie;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * MovieController
 *
 * @author zhouxiang
 * @date 2021/12/30 1:45 下午
 */
@RestController
@RequestMapping("/movie")
public class MovieController {

    @GetMapping("/{id}")
    public String getById(@PathVariable Long id) {
        System.out.println("id:" + id);
        return new Movie(id, "电影：" + id).toString();
    }

}
